# BankApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Notes About The Project

>> For the withdrawal i have a method like below 

>>Please note that i pass in the value 100, the amount the user wants to withdraw,
>>this amount can be captured from a form or an input filed, promting the user how much they want to withdraw,
>>in this case i have hard Coded as 100. 

## for testing purposes the value can be changed to any amount, to test all the business rules

 setSelectedAccount(index) {
    this.selectedAccount = this.accounts[index];
    const res = this.accountsService.withdraw(this.selectedAccount,100);
    if (!res) {
      alert('Not Successfull');
    }   else {
      alert('Successfull');
    }
  }
