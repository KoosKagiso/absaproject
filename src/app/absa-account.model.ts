export class AbsaAccount {
   public account_number: number;
   public account_type: string;
   public balance: number = 0;

   constructor(protected account_balance,protected accountType, protected accBalance) {
       this.account_balance = account_balance;
       this.account_type = accountType;
       this.balance = accBalance;

    }
  
   public getAccountType() {
        return this.account_type;
    }
}


