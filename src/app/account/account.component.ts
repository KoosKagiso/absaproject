import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
import {AccountsService} from '../accounts.service';
import {AbsaAccount} from '../absa-account.model';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers: [AccountsService]
})
export class AccountComponent implements OnInit {
  accounts: AbsaAccount[];
  title = 'Acne Bank';
  currenySymbol = "R";
  subTittle = 'Account List';
  selectedAccount: AbsaAccount;
  total = 0;
  constructor(private accountsService: AccountsService, private eleRef: ElementRef) {

   }

   getAccounts(): void {
    
    this.accountsService.getAccounts()
        .subscribe (
            response => {
              this.accounts = response;
              this.total = this.accountsService.getTotal(this.accounts);
            },            
            error => console.log('Error :: ' + error)
        );

  }

  setSelectedAccount(index) {
    this.selectedAccount = this.accounts[index];
    const res = this.accountsService.withdraw(this.selectedAccount,100);
    if (!res) {
      alert('Not Successfull');
    }   else {
      alert('Successfull');
    }
  }

  isWithdrwal(account) {    
    const result = this.accountsService.isWithdrawal(account);
    return result;
  }

  ngOnInit(): void {
    this.getAccounts();
    
   }
}
