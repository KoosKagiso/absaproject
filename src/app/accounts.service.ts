import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {AbsaAccount} from './absa-account.model';
import {SavingsAccount} from './savings-account';
import {CheaqueAccount} from './cheaque-account';

@Injectable()
export class AccountsService {

  private _apiUrl = 'http://localhost:8080/api/accounts';
  cheaque: CheaqueAccount;
  savings: SavingsAccount;

  constructor(
    private http: Http,
  ) {
  }

  getAccounts(): Observable<AbsaAccount[]> {
    return this.http
      .get(this._apiUrl)
      .map((response: Response) => {
        return <AbsaAccount[]>response.json().accounts;
      })
      .catch(this.handleError);
  }

  withdraw(a: AbsaAccount, amount: number): Boolean {
    if (a.account_type === 'savings') {
      this.savings = new SavingsAccount(a.account_number,a.account_type,a.balance);
      return this.savings.withdraw(amount);
    } else {
      this.cheaque = new CheaqueAccount(a.account_number, a.account_type, a.balance);
      return this.cheaque.withdraw(amount);
    }
  }

  isWithdrawal(a: AbsaAccount): Boolean {
   if (a.account_type === 'cheque') {
     if (a.balance < -500) {
       return true;
     }
   }
   else if (a.account_type === 'savings') {
        if (a.balance < 0) {
          return true;
        }
   }
  }

  getTotal(accounts:AbsaAccount[]) {
    let total:any = 0;
    let balancess: any;
    for ( let i = 0; i < accounts.length; i++) {   
     balancess = accounts[i].balance;
     total = parseFloat(balancess) + total;
    }
    return total;
  }

  private handleError(error: Response) {
    return Observable.throw(error.statusText);
  }
}
