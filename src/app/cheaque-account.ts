import {AbsaAccount} from './absa-account.model';

export class CheaqueAccount extends AbsaAccount {

    constructor( account_number,protected accountType, protected accBalance) {
        super(account_number, accountType , accBalance );
    }

    withdraw(amount: number) {
       if (this.balance < -500 || this.balance - amount < -500 ) {
            console.log('ERROR : Overdraft exceded');
            return false;
        } else {
            this.balance = this.balance - amount;
            return true;
        }
    }
}
