import {AbsaAccount} from "./absa-account.model";

export class SavingsAccount extends AbsaAccount {
    constructor( account_number,protected accountType, protected accBalance) {
        super(account_number, accountType , accBalance );
    }

    withdraw(amount: number)
    {
         if(this.balance < 0 || this.balance - amount < 0) {
            console.log("ERROR : you cannot withdraw");
            return false;
        }
        else {
            this.balance = this.balance - amount;
            alert("remaining balance its " +  this.balance)
            console.log("savings balance" +  this.balance);
            return true;
        }
     
    }
}
